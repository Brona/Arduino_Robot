uint8_t pinSBut = 2;  //Pin of start button.
uint8_t pinLSpd = 10; //Pin for PWM of left motor.
uint8_t pinLFwd = 9;  //Pin for froward movement of left motor.
uint8_t pinLBwd = 8;  //Pin for backward movement of left motor.
uint8_t pinRFwd = 7;  //Pin for froward movement of right motor.
uint8_t pinRBwd = 6;  //Pin for backward movement of right motor.
uint8_t pinRSpd = 5;  //Pin for PWM of right motor.

int spd = 255;

double optSpd[13][2] = {
//   Optimalization for L gears.
//         Optimalization for R gears.
    {  0.0,  0.0}, // 0
    
    { -1.0, -1.0}, // 1
    {  1.0,  1.0}, // 2
    {  1.0, -1.0}, // 3
    { -1.0,  1.0}, // 4

    {  1.0,  1.0}, // 5
    {  0.0,  0.0}, // 6
    {  0.0,  1.0}, // 7
    {  1.0,  0.0}, // 8

    {  1.0,  1.0}, // 9
    {  0.5,  0.5}, // 10
    { 0.25, 0.25}, // 11
    {  0.1,  0.1}, // 12
};

void SetSpd(int lineOfArray) { //Set speed and direction.
  if (optSpd[lineOfArray][0] >= 0.0) {
    digitalWrite(pinLBwd, LOW); //Left gears forward.
    digitalWrite(pinLFwd, HIGH);
  } else {
    digitalWrite(pinLFwd, LOW); //Left gears backward.
    digitalWrite(pinLBwd, HIGH);
  }

  if (optSpd[lineOfArray][1] >= 0.0) {
    digitalWrite(pinRBwd, LOW); //Right gears forward.
    digitalWrite(pinRFwd, HIGH);
  } else {
    digitalWrite(pinRFwd, LOW); //Right gears backward.
    digitalWrite(pinRBwd, HIGH);
  }

  analogWrite(pinLSpd, (int) round(abs(optSpd[lineOfArray][0]) * spd)); //Left gears speed set.
  analogWrite(pinRSpd, (int) round(abs(optSpd[lineOfArray][1]) * spd)); //Right gears speed set.
}

void setup() {
  pinMode(pinLFwd, OUTPUT);
  pinMode(pinLBwd, OUTPUT);
  pinMode(pinRFwd, OUTPUT);
  pinMode(pinRBwd, OUTPUT);

  Serial.begin(19200);
  Serial.print("Serial begin!\n\n");
}

void loop() {
  
  Serial.print("Direction control!\n\n");
  
  SetSpd(1);
  delay(500);
  SetSpd(2);
  delay(500);
  SetSpd(3);
  delay(500);
  SetSpd(4);
  delay(500);

  SetSpd(0);
  delay(1000);

  Serial.print("Different speed control!\n\n");

  SetSpd(5);
  delay(500);
  SetSpd(6);
  delay(500);
  SetSpd(7);
  delay(500);
  SetSpd(8);
  delay(500);

  SetSpd(0);
  delay(1000);

  Serial.print("Slowing control!\n\n");
  
  SetSpd(9);
  delay(500);
  SetSpd(10);
  delay(500);
  SetSpd(11);
  delay(500);
  SetSpd(12);
  delay(500);
  SetSpd(11);
  delay(500);
  SetSpd(10);
  delay(500);
  SetSpd(9);
  delay(500);
  
  SetSpd(0);
  delay(1000);

}
