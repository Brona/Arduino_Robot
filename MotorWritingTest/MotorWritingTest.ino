uint8_t pinSBut = 2;  //Pin of start button.
uint8_t pinLSpd = 10; //Pin for PWM of left motor.
uint8_t pinLFwd = 9;  //Pin for froward movement of left motor.
uint8_t pinLBwd = 8;  //Pin for backward movement of left motor.
uint8_t pinRFwd = 7;  //Pin for froward movement of right motor.
uint8_t pinRBwd = 6;  //Pin for backward movement of right motor.
uint8_t pinRSpd = 5;  //Pin for PWM of right motor.

int spd = 255;

double optSpd[1][2] = {
//   Optimalization for L gears.
//         Optimalization for R gears.
    {  0.0,  0.0}, // 0
};

void SetSpd(int lineOfArray) { //Set speed and direction.
  if (optSpd[lineOfArray][0] >= 0.0) {
    digitalWrite(pinLBwd, LOW); //Left gears forward.
    digitalWrite(pinLFwd, HIGH);
  } else {
    digitalWrite(pinLFwd, LOW); //Left gears backward.
    digitalWrite(pinLBwd, HIGH);
  }

  if (optSpd[lineOfArray][1] >= 0.0) {
    digitalWrite(pinRBwd, LOW); //Right gears forward.
    digitalWrite(pinRFwd, HIGH);
  } else {
    digitalWrite(pinRFwd, LOW); //Right gears backward.
    digitalWrite(pinRBwd, HIGH);
  }

  analogWrite(pinLSpd, (int) round(abs(optSpd[lineOfArray][0]) * spd)); //Left gears speed set.
  analogWrite(pinRSpd, (int) round(abs(optSpd[lineOfArray][1]) * spd)); //Right gears speed set.
}

void setup() {
  pinMode(pinLFwd, OUTPUT);
  pinMode(pinLBwd, OUTPUT);
  pinMode(pinRFwd, OUTPUT);
  pinMode(pinRBwd, OUTPUT);

  Serial.begin(19200);
  Serial.print("Serial begin!\n\n");
}

void loop() {
  for(double d = 0.0; d < 1.01; d += 0.05) {
    optSpd[0][0] = d;
    optSpd[0][1] = d;
    SetSpd(0);
    String toPrint = "Double d is now: ";
    Serial.print(toPrint + d + ".\n");
    delay(10000);
  }
}
