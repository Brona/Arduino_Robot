int cekani_v_sec = 2;

void setup() {
  Serial.begin(19600);
  delay(100);
}

void loop() {
    
  Serial.println(" Hodnota na senzorech: ");
  Serial.println("---------------------------------");

  Serial.println();
  
  Serial.print("1: ");
  Serial.print(analogRead(A0));
 
  Serial.print(",   2: ");
  Serial.print(analogRead(A1));
 
  Serial.print(",   3: ");
  Serial.print(analogRead(A2));
 
  Serial.print(",   4: ");
  Serial.print(analogRead(A3));
 
  Serial.print(",   5: ");
  Serial.print(analogRead(A4));
 
  Serial.print(",   6: ");
  Serial.print(analogRead(A5));
  
  Serial.print(",   7: ");
  Serial.print(analogRead(A6));
 
  Serial.print(",   8: ");
  Serial.println(analogRead(A7));

  Serial.println();

  Serial.print("(Automaticky obnovováno po ");
  Serial.print(cekani_v_sec);
  Serial.println(" sekundách)");

  Serial.println();
  Serial.println();
  Serial.println();
  Serial.println();
  Serial.println();
  Serial.println();
  Serial.println();
  Serial.println();
  
  delay(cekani_v_sec * 1000);
}

